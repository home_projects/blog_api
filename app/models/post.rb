class Post < ActiveRecord::Base

  belongs_to :user
  has_many :user_post_ratings, dependent: :destroy

  # post -> raitng (высчитанны средний чтобы отдавать)
  # при оценке
  # если не оценивал еще
  # сохраняем в users_posts_ratings user_id, post_id, rating
  # по формуле (prev_ratings_summ + новая оценка) / (prev_ratings_count + 1) поллучаем новый средний рейтинг
  # сохраняем в post
  #   rating
  #   prev_ratings_summ = (prev_ratings_summ + новая оценка)
  #   prev_ratings_count = prev_ratings_count + 1

  def ip_string
    IPAddr.new(author_ip, Socket::AF_INET).to_s
  end

  def recalculate_new_average_rating_and_save rating
    new_ratings_summ = self.prev_ratings_summ + rating
    new_ratings_count = self.prev_ratings_count + 1
    new_rating = new_ratings_summ / new_ratings_count

    self.rating = new_rating
    self.prev_ratings_summ = new_ratings_summ
    self.prev_ratings_count = new_ratings_count
    self.save
  end

  def self.top_rating_list(n)
    all.order('rating desc').limit(n)
  end
end
