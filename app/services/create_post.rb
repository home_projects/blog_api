class CreatePost
  attr_reader :errors
  attr_reader :post

  def initialize(params, request, user)
    @params = params
    @request = request
    @user = user
    @errors = []
    create_post
  end

  def create_post
    @success = false

    if valid_params?
      @post = @user.posts.build(post_params)
      if @post.save
        @success = true
      else
        @errors << post.errors
      end
    end
  end

  def valid_params?
    @errors << {name: "can't be blank"} unless post_params[:name].present?
    @errors << {body: "can't be blank"} unless post_params[:body].present?
    @errors.empty?
  end

  def success?
    @success
  end

  private

  def post_params
    data = @params.fetch(:post, {}).permit(:name, :body)
    # seed для записи разных фэйк адресов
    data[:author_ip] = @params[:seed].present? ? @params[:post][:author_ip].to_i : IPAddr.new(@request.remote_ip).to_i
    data
  end

end