class GetAuthorsWithSameIp
  attr_reader :data
  attr_reader :errors

  def initialize()
    @data = []
    @errors = []
    get_data
  end

  def get_data
    @success = false

    sql = "SELECT p.author_ip, p.user_id, u.email FROM posts p JOIN users u ON p.user_id=u.id GROUP BY p.author_ip, p.user_id, u.email"

    begin
      uniq_pairs = Post.find_by_sql sql
    rescue => e
      @errors << e.message
    end

    if @errors.empty?
      @data = data_format uniq_pairs
      @success = true
    end
  end

  def group_by_author_ip uniq_pairs
    uniq_pairs.group_by{|h| h[:author_ip]}
  end

  def data_format uniq_pairs
    grouped_by_author_ip = group_by_author_ip uniq_pairs
    grouped_by_author_ip.map {|k,v| { ip: k, login: v.map(&:email)} if v.length > 1}.reject(&:nil?)
  end

  def success?
    @success
  end

end