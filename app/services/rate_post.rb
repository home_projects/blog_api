class RatePost
  attr_reader :errors
  attr_reader :post
  attr_reader :rating

  def initialize(params, user, post)
    @params = params
    @user = user
    @post = post
    @errors = []
    @user_rating_to_i = post_params[:rating].to_i
    create_rating
  end

  def create_rating
    @success = false

    if valid_params?

      begin
        upr = UserPostRating.new(post_id: @post.id, user: @user, rating: @user_rating_to_i)
        upr.save
      rescue ActiveRecord::RecordNotUnique
        @errors << {rating: 'This user already has rated this post.'}
      end

      if @errors.empty?
        # по формуле post (prev_ratings_summ + новая оценка) / prev_ratings_count + 1 поллучаем новый средний рейтинг
        @post.recalculate_new_average_rating_and_save @user_rating_to_i
        @success = true
      end

    end

  end

  def valid_params?
    @errors << {id: "post_id can't be blank"} unless @params[:id].present?
    @errors << {rating: "can't be blank"} unless post_params[:rating].present?
    @errors << {rating: "must be in range from 1 to 5"} unless post_params[:rating].to_i.in? [*1..5]
    @errors.empty?
  end

  def success?
    @success
  end

  private

  def post_params
    @params.fetch(:post, {}).permit(:rating)
  end
end