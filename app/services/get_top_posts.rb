class GetTopPosts
  attr_reader :data
  attr_reader :errors

  def initialize(count)
    @count = count
    @data = []
    @errors = []
    get_data
  end

  def get_data
    @success = false

    begin
      @data = Post.top_rating_list(@count)
    rescue => e
      @errors << e.message
    end

    if @errors.empty?
      @success = true
    end
  end

  def success?
    @success
  end

end