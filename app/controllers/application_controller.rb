class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  # seed, tests - without sessions, find user by user_id
  def current_user
    if (params[:seed].present? || params[:test].present?) && params[:user_id].present?
      User.find(params[:user_id].to_i)
    else
      super
    end
  end

  def authenticate_user!
    # seed, tests - without sessions
    unless (params[:seed].present? || params[:test].present?)
      render json: {errors: 'You need to login first'} unless current_user
    end
  end

end
