class Users::SessionsController < Devise::SessionsController

  respond_to :json

  # POST /resource/sign_in
  # curl -H "Content-Type: application/json" -X POST -d '{"user":{"email":"user1@domain.com", "password":"11111111"}}' http://localhost:3000/users/sign_in
  def create
    errors = []
    email, password = [params[:user][:email], params[:user][:password]]
    if email.present? && password.present?
      unless User.find_by_email(email)
        u = User.new(email: email, password: 11111111, password_confirmation: 11111111)
        unless u.save
          errors = u.errors
        end
      end
    end

    if errors.empty?
      self.resource = warden.authenticate!(auth_options)
      sign_in(resource_name, resource)
      yield resource if block_given?

      respond_to do |format|
        format.json {render json: {success: true}, status: 200}
      end
    else
      respond_to do |format|
        format.json {render json: {errors: errors}, status: 401}
      end
    end
  end

end
