class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_post, only: [:rate]

  # login
  # curl -H "Content-Type: application/json" -X POST -d '{"user":{"email":"user1@domain.com", "password":"11111111"}}' http://localhost:3000/users/sign_in

  def index
    render text: 'Welcome'
  end

  # curl -H "Content-Type: application/json" -X POST -d '{"post":{"name":"post_name", "body":"post_body"}, "test":"true", "user_id":"1"}' http://localhost:3000/posts
  def create
    result =  CreatePost.new(params, request, current_user)

    respond_to do |format|
      format.json {
        if result.success?
          render json: {post: result.post}
        else
          render json: {errors: result.errors}, status: 422
        end
      }
    end
  end

  # curl -H "Content-Type: application/json" -X POST -d '{"post":{"rating":"5"}, "test":"true", "user_id":"1"}' http://localhost:3000/posts/1/rate
  def rate
    result =  RatePost.new(params, current_user, @post)

    respond_to do |format|
      format.json {
        if result.success?
          render json: {average_rating: result.post.rating}
        else
          render json: {errors: result.errors}, status: 422
        end
      }
    end
  end

  # curl -H "Content-Type: application/json" http://localhost:3000/posts/top_list?test=true
  def top_list
    result = GetTopPosts.new(10)

    respond_to do |format|
      format.json {
        if result.success?
          render json: {data: result.data.select(:name, :body, :rating)}
        else
          render json: {errors: result.errors}, status: 422
        end
      }
    end
  end

  # curl -H "Content-Type: application/json" http://localhost:3000/posts/ip_authors?test=true
  def ip_authors
    result = GetAuthorsWithSameIp.new()

    respond_to do |format|
      format.json {
        if result.success?
          render json: {data: result.data}
        else
          render json: {errors: result.errors}, status: 422
        end
      }
    end
  end

  private
    def set_post
      @post = Post.find(params[:id])
    end

end
