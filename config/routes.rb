Rails.application.routes.draw do
  root 'posts#index'

  devise_for :users, controllers: { sessions: 'users/sessions'}

  resources :posts, only: [:index, :create] do
    collection do
      get :top_list
      get :ip_authors
    end
    member do
      post :rate
    end
  end
end
