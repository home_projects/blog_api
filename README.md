Детали:
Я не писал чистого api ранее.
Я смотрел различные описания того как должно быть устроено api only приложение в rails,
но я не уверен что так с ходу сделал бы все правильно и быстро.
Поэтому я постарался реализовать основную логику в стандартном rails приложении с ответами на формате json.
Также есть проблема с логином при тестах запросов с помощью curl в консоли.
Юзер логинится на сервере. Но при последующих запросах приложение не знает что я залогинен.
Я подумал о запросах с токеном полученным после авторизации. Я попробовал использовать devise_token_auth (https://github.com/lynndylanhurley/devise_token_auth)
но в этом случае токен меняется при каждом запросе и во всех новых запросах нужно в хедере отсылать каждый раз разные параметры. Тестировать неудобно.
Я бы мог например сам генерить токен после логина и потом им подписывать все новые запросы, но не уверен, что такой способ использовался бы в продакшне.
В итоге для тестов я оставил некий костыль - нужно передавать параметр test=true в тестовых запросах или seed=true (для сидов) и user_id  в случае когда нужен авторизованный юзер.

SQL задача:
Я не часто сталкивался с чистым SQL поэтому я впринципе не знал что такое непрерывные группы.
Из информации что я нашел мне стало понятно на таком примере:
нужно получить колчество записей когда юзер заходил на ресурс каждый день непрерывно.
То есть даты входа возрастают на 1 день последовательно.

В заданной задаче мне не понятно по какому критерию(логически) нужно выделить группы.
create temp table users(id bigserial, group_id bigint);
insert into users(group_id) values (1), (1), (1), (2), (1), (3);
В этой таблице, упорядоченой по ID необходимо:
выделить непрерывные группы по group_id с учетом указанного порядка записей (их 4)
подсчитать количество записей в каждой группе
вычислить минимальный ID записи в группе

Нам нужно выделить в группы с подряд идущим одинаковым group_id(повторяющимся либо нет) так например: 111 / 2 / 1 / 3 
Или как-от иначе. Непонятна цель и логика. Возможно потому что у меня в этом пробел.

Мне интересно с этим разобраться. Если моя реализация задания вам хоть как-то интересна напишите пожалуйста.
И про логику выделения в группы.

stassavickii@gmail.com