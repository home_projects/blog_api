# Рассчитаны на запуск один раз на нулевой базе

# авторов ~ 100
user_count = 100
user_count.times do |i|
  User.create(email: "user#{i+1}@domain.com",password: 11111111, password_confirmation: 11111111)
end

# ip ~ 50
ips = 50.times.map {IPAddr.new(rand(2**32), Socket::AF_INET).to_i}

# todo
# Постов > 200к
posts_count = 300000
posts_count.times do |i|
  `curl --data "seed=true&post[name]=post_name_#{i}&post[body]=post_body_#{i}&post[author_ip]=#{ips[rand(0..49)]}&user_id=#{rand(1..user_count)}" localhost:3000/posts`
end

# Часть постов должна получить оценки
(posts_count/2).times do |i|
  `curl --data "seed=true&post[rating]=#{rand(1..5)}&user_id=#{rand(1..user_count)}" localhost:3000/posts/#{rand(1..posts_count)}/rate`
end




