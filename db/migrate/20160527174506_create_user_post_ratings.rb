class CreateUserPostRatings < ActiveRecord::Migration
  def up
    create_table :user_post_ratings do |t|
      t.references :user, null: false
      t.references :post, null: false
      t.integer :rating, null: false
      t.timestamps null: false
    end

    add_index :user_post_ratings, [:user_id, :post_id], :unique => true
  end

  def down
    drop_table :user_post_ratings
  end
end
