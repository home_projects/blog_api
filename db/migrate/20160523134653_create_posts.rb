class CreatePosts < ActiveRecord::Migration
  def up
    create_table :posts do |t|
      t.string :name, null: false
      t.text :body, null: false
      t.column :author_ip, :bigint, null: false
      t.integer :rating, null: false, default: 0, index: true
      t.integer :prev_ratings_summ, null: false, default: 0
      t.integer :prev_ratings_count, null: false, default: 0
      t.references :user, null: false, index: true
      t.timestamps null: false
    end

    add_index :posts, :created_at
  end

  def down
    drop_table :posts
  end
end
