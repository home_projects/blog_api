RSpec.describe User, :type => :model do
  it "can be created" do
    user_1 = User.create!(email: "xxx@mail.ru", password: '11111111', password_confirmation: '11111111')
    user_2 = User.create!(email: "yyy@mail.ru", password: '11111111', password_confirmation: '11111111')

    expect(User.all.order('id asc')).to eq([user_1, user_2])
  end
end