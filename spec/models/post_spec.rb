RSpec.describe Post, :type => :model do
  it "can be created" do
    Post.create(name: 'post_name', body: 'post_body', user_id: 1, author_ip: IPAddr.new('12.34.56.78').to_i)
    expect(Post.find_by_body('post_body').present?).to be true
  end

  it "post belongs_to user" do
    user = FactoryGirl.create(:user)
    post = FactoryGirl.create(:post, body: 'post_body', user: user)
    expect(Post.find_by_body('post_body').user).to eq user
  end

end