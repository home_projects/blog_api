FactoryGirl.define do

  factory :user do
    sequence :email do |n|
      "user#{n}@domain.com"
    end
    password '11111111'
    password_confirmation '11111111'
  end

  factory :post do
    sequence :name do |n|
      "post_#{n}"
    end

    sequence :body do |n|
      "post_body_#{n}"
    end

    user
    author_ip IPAddr.new('12.34.56.78').to_i
  end

end


