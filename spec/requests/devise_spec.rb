include TestHelpers

describe "Devise" do

  context 'Existing user' do

    it 'can be logged in' do
      user = FactoryGirl.create(:user)

      post '/users/sign_in',  { format: :json, user: {email: user.email, password: '11111111'} }
      json = JSON.parse(response.body)

      expect(response).to be_success
      expect(json['success']).to be true
    end

    it 'cant login with wrong pass' do
      user = FactoryGirl.create(:user)

      post '/users/sign_in',  { format: :json, user: {email: user.email, password: '1'} }
      json = JSON.parse(response.body)

      expect(response.status).to eq 401
      expect(json['error']).to eq 'Invalid email or password.'
    end

    it 'cant login with wrong email' do
      user = FactoryGirl.create(:user)

      post '/users/sign_in',  { format: :json, user: {email: 'wrong.email', password: '11111111'} }
      json = JSON.parse(response.body)

      expect(response.status).to eq 401
      expect(json['errors']['email'][0]).to eq 'is invalid'
    end

  end

end




