include TestHelpers

describe "Post" do

  context 'User with session' do
    let!(:user) {sign_in FactoryGirl.create(:user)}

    it 'can create post' do

      post '/posts', { format: :json, post: {name: 'post_name', body: 'post_body'} }
      json = JSON.parse(response.body)

      expect(response).to be_success
      expect(json['post']['name']).to eq 'post_name'
      expect(json['post']['body']).to eq 'post_body'
      expect(json['post']['user_id']).to eq user.id
      expect(json['post']['rating']).to eq 0
      expect(json['post']['prev_ratings_summ']).to eq 0
      expect(json['post']['prev_ratings_count']).to eq 0

      expect(IPAddr.new(json['post']['author_ip'], Socket::AF_INET).to_s).to eq '127.0.0.1'
    end

    it 'cant create post without required params' do
      # without body
      post '/posts', { format: :json, post: {name: 'post_name'} }
      json = JSON.parse(response.body)
      expect(response.status).to eq 422
      expect(json['errors'][0]['body']).to eq "can't be blank"

      # without name
      post '/posts', { format: :json, post: {body: 'post_body'} }
      json = JSON.parse(response.body)
      expect(response.status).to eq 422
      expect(json['errors'][0]['name']).to eq "can't be blank"
    end

    context 'New post with rating: 0, prev_ratings_summ: 0, prev_ratings_count: 0' do

      it 'can rate post' do
        post = FactoryGirl.create(:post)

        post "/posts/#{post.id}/rate", { format: :json, post: {rating: 2} }

        json = JSON.parse(response.body)

        expect(response).to be_success
        expect(json['average_rating']).to eq 2
        post.reload
        expect(post.rating).to eq 2
        expect(post.prev_ratings_summ).to eq 2
        expect(post.prev_ratings_count).to eq 1
      end

      it 'user cant rate post more than 1 time' do
        post = FactoryGirl.create(:post)

        # first rate
        post "/posts/#{post.id}/rate", { format: :json, post: {rating: 2} }

        json = JSON.parse(response.body)

        expect(response).to be_success
        expect(json['average_rating']).to eq 2

        # next rate from same user
        post "/posts/#{post.id}/rate", { format: :json, post: {rating: 5} }
        json = JSON.parse(response.body)
        expect(response.status).to eq 422
        expect(json['errors'][0]['rating']).to eq 'This user already has rated this post.'
      end

      it 'cant set rating out of range 1..5' do
        post = FactoryGirl.create(:post)

        post "/posts/#{post.id}/rate", { format: :json, post: {rating: 100} }

        json = JSON.parse(response.body)

        expect(response.status).to eq 422
        expect(json['errors'][0]['rating']).to eq 'must be in range from 1 to 5'
      end

    end

    context 'With posts' do
      10.times do |i|
        number = i+1
        let!("post#{number}") { FactoryGirl.create(:post, rating: (number)) }
      end

      it 'can get top rated posts' do

        get '/posts/top_list', { format: :json }

        json = JSON.parse(response.body)

        top_ratings_array = json['data'].map{ |k| k['rating']}

        expect(response).to be_success
        expect(top_ratings_array).to eq [*1..10].reverse
      end

    end

    context 'With posts and authors that created posts from same ip' do
      # user1 created 10 posts with ip#1
      # user2 created 10 posts with ip#1

      # user3 created 10 posts with ip#2
      # user4 created 1  post  with ip#2

      # user5 created 50 post  with ip#3

      # user6 created 1  post  with ip#4
      # user6 created 1  post  with ip#5
      # user6 created 1  post  with ip#6

      6.times do |i|
        number = i+1
        let!("user_#{number}") { FactoryGirl.create(:user, email: "user_#{number}@domain.com")}
      end

      ips = ['128.0.0.0',
             '128.0.0.1',
             '127.0.0.0',
             '127.1.1.1',
             '125.1.1.1',
             '124.1.1.1'].map{|ip| IPAddr.new(ip).to_i}

      10.times do |i|
        number = i+1
        let!("user_1_post_#{number}") { FactoryGirl.create(:post, user: user_1, author_ip: ips[0]) }
        let!("user_2_post_#{number}") { FactoryGirl.create(:post, user: user_2, author_ip: ips[0]) }

        let!("user_3_post_#{number}") { FactoryGirl.create(:post, user: user_3, author_ip: ips[1]) }
      end

      let!("user_4_post_1") { FactoryGirl.create(:post, user: user_4, author_ip: ips[1]) }

      50.times do |i|
        number = i+1
        let!("user_5_post_#{number}") { FactoryGirl.create(:post, user: user_5, author_ip: ips[2]) }
      end

      let!("user_6_post_1") { FactoryGirl.create(:post, user: user_6, author_ip: ips[3]) }
      let!("user_6_post_2") { FactoryGirl.create(:post, user: user_6, author_ip: ips[4]) }
      let!("user_6_post_3") { FactoryGirl.create(:post, user: user_6, author_ip: ips[5]) }


      it 'can get list of different authors that created posts from same ip' do

        get '/posts/ip_authors', { format: :json }

        json = JSON.parse(response.body)

        results = []
        json['data'].each do |hash|
          emails = hash['login']

          if hash['ip'] == ips[0]
            is_equal = [user_1.id, user_2.id].sort == User.where(email: emails).pluck(:id).sort
            results << is_equal
          else
            is_equal = [user_3.id, user_4.id].sort == User.where(email: emails).pluck(:id).sort
            results << is_equal
          end
        end

        expect(response).to be_success
        expect(json['data'].length).to eq 2
        expect(results.uniq.length).to eq 1
        expect(results.uniq.first).to be true
      end

    end

  end

  context 'User without session' do
    it 'Rate Calculation is right after every rate' do
      post = FactoryGirl.create(:post)

      # ratings = [2]
      # ratings = [2, 4, 6]
      ratings = [1,2,3,4,5]


      ratings.each_with_index do |rating, i|

        # на каждом щаге будет оценивать новый залогиненный юзер
        new_user =  sign_in FactoryGirl.create(:user)

        post "/posts/#{post.id}/rate", { format: :json, post: {rating: rating} }

        # binding.pry

        # для будущего быстрого подсчета среднего рейтинга нам нужны параметры:

        # текущая сумма рейтингов -> избегаем выборки всех рейтингов из UserPostRating и их суммирования в будущем
        current_ratings_summ = ratings.first(i+1).sum

        # и текущее количество оценок
        current_ratings_count = i+1

        # средний рейтинг по текущим данным обчно рассчитывается
        average_rating = current_ratings_summ/current_ratings_count

        # но мы считаем в реальности, используя post.prev_ratings_summ и post.prev_ratings_count
        # по формуле  (prev_ratings_summ + новая оценка) / prev_ratings_count + 1

        # Проверяем:

        # что наш подсчет соответствует стандартному расчету по формуле выше

        # binding.pry
        json = JSON.parse(response.body)
        post.reload

        expect(response).to be_success
        expect(json['average_rating']).to eq average_rating
        expect(post.rating).to eq average_rating

        # что данные для следующего расчета сохранены в post.prev_ratings_summ и post.prev_ratings_count
        expect(post.prev_ratings_summ).to eq current_ratings_summ
        expect(post.prev_ratings_count).to eq current_ratings_count


        # также проверим что средний рейтинг высчитанный по нашей формуле соответствует среднему рейтингу, основанному на всех оценках поста
        # мы их храним стандартно в (UserPostRating)
        all_ratings = UserPostRating.where(post_id: post.id)

        ratings_summ = all_ratings.pluck(:rating).sum
        ratings_count = all_ratings.count
        average_rating_by_data = ratings_summ/ratings_count

        expect(post.rating).to eq average_rating_by_data
      end

    end
  end


end




