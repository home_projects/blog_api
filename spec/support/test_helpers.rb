include Warden::Test::Helpers

module TestHelpers
  def sign_in(user)
    login_as user, scope: :user
    user
  end
end
